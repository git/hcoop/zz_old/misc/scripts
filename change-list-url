#!/bin/bash
#
# Change a mailing list's web page URL.
#
# This must be run as root.

MMDIR=/var/lib/mailman
BINDIR=$MMDIR/bin
LISTDIR=$MMDIR/lists
WORKDIR=/tmp/mmdir

function usage() {
    echo "Usage: change-list-url LIST WEBURL [BASEURL]"
}

# Argument check
if test -z "$2"; then
    echo "Not enough arguments"
    usage
    exit 1
elif test -n "$4"; then
    echo "Too many arguments"
    usage
    exit 1
else
    LIST=$1
    WEBURL=$2
    BASEURL=$3
fi

# Check list existence
if test ! -d $LISTDIR/$LIST; then
    echo "List $LIST does not exist"
    exit 1
fi

# Make working directory
if test ! -d $WORKDIR; then
    mkdir -p $WORKDIR
    chown list:list $WORKDIR
    chmod ug=rwX,o= $WORKDIR
fi

# Change URL
echo "web_page_url = '$WEBURL'" >  $WORKDIR/$LIST.cfg
if test -n "$BASEURL"; then
    echo "host_name = '$BASEURL'" >>  $WORKDIR/$LIST.cfg
fi
chown list:list $WORKDIR/$LIST.cfg
sudo -u list $BINDIR/config_list -i $WORKDIR/$LIST.cfg $LIST
rm -f $WORKDIR/$LIST.cfg

echo
echo "Changed web page URL for $LIST to $WEBURL"
if test -n "$BASEURL"; then
    echo "  and also base URL to $BASEURL"
fi

echo "Updating mailman definitions for Exim ..."
/afs/hcoop.net/common/etc/scripts/mailman-update-exim-db
