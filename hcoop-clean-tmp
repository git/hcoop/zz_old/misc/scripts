#!/bin/sh

# Thu Mar 27 13:59:00 EDT 2008 docelic:
#
# Clean /tmp for files older than TMPTIME days. Basically the copy of
# /etc/init/bootclean's clean_tmp().
#
# How much files are kept is controlled by existing variable TMPTIME in
# /etc/default/rcS.
#

umask 022

. /lib/init/vars.sh
. /lib/lsb/init-functions

VERBOSE=no

cd /tmp || { log_failure_msg "bootclean: Could not cd to /tmp." ; return 1 ; }

[ "$(find . -maxdepth 0 -perm -002)" = "." ] || return 0

if [ ! "$TMPTIME" ]
then
	log_warning_msg "Using default TMPTIME 0."
	TMPTIME=0
fi

[ "$VERBOSE" = no ] || log_action_begin_msg "Cleaning /tmp"

rm -f .X*-lock

#
# Don't clean remaining files if TMPTIME is negative or 'infinite'
#
case "$TMPTIME" in
  -*|infinite|infinity)
	[ "$VERBOSE" = no ] || log_action_end_msg 0 "skipped"
	return 0
	;;
esac

if [ "$TMPTIME" = 0 ] 
then
	TEXPR=""
	DEXPR=""
else
	TEXPR="-mtime +$TMPTIME -ctime +$TMPTIME -atime +$TMPTIME"
	DEXPR="-mtime +$TMPTIME -ctime +$TMPTIME"
fi

EXCEPT='! -name .
	! ( -path ./lost+found -uid 0 )
	! ( -path ./quota.user -uid 0 )
	! ( -path ./aquota.user -uid 0 )
	! ( -path ./quota.group -uid 0 )
	! ( -path ./aquota.group -uid 0 )
	! ( -path ./.journal -uid 0 )
	! ( -path ./.clean -uid 0 )
	! ( -path ./lost+found -uid 0 )
	! ( -path './...security*' -uid 0 )'

report_err()
{
	if [ "$VERBOSE" = no ]
	then
		log_failure_msg "bootclean: Failure cleaning /tmp."
	else
		log_action_end_msg 1 "bootclean: Failure cleaning /tmp"
	fi
}

#
# First remove all old files...
# (Use xargs here so that only one additional process gets created)
#
find . -depth -xdev $TEXPR $EXCEPT ! -type d \
	-print0 | xargs -0r rm -f -- \
	|| { report_err ; return 1 ; }

#
# ...and then all empty directories
# (Don't use xargs here because dirs must be removed one by one from
# the bottom up)
#
find . -depth -xdev $DEXPR $EXCEPT -type d -empty \
	-exec rmdir \{\} \; \
	|| { report_err ; return 1 ; }

[ "$VERBOSE" = no ] || log_action_end_msg 0

exit 0


